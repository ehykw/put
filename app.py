#!/usr/bin/env python
#-*- coding: utf-8 -*-
import os
from flask import Flask
from flask import request
from flask import render_template
from flask import abort, redirect, url_for

data = 0

app = Flask(__name__)

# buttonファイルを表示する
@app.route('/')
def index():
    return render_template('index.html', val=data)

# ボタンの処理
@app.route('/put', methods=['GET', 'PUT'])
def ledon():
    global data
    if request.method == 'PUT':
        # POSTメソッドならled_statusに現在のLED状態を保持
        data = request.form['data']
        print data
        return redirect(url_for('index'))
    elif request.method == 'GET':
        # GETメソッドならled_statusの値を返す
        return data
    else:
        return redirect(url_for('index'))

if __name__ == '__main__':
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True)
